server {
    include snippets/listen-tls.conf;
    server_name example.com;

    root /srv/http/example.com;

    include snippets/standard-server.conf;
}

server {
    include snippets/listen-http.conf;
    server_name example.com;

    return 301 https://example.com$request_uri;
}
